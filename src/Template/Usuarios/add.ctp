<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link( ('Listar Marcadores'), ['controller' => 'Marcadores', 'action' => 'index'])
        ?></li>
    </ul>
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Agregar Usuario') ?></legend>
        <?php
            echo $this->Form->input('username');
            echo $this->Form->input('email');
        if (isset($rol) && $rol == 'administrador') {
            echo $this->form->input('rol',['options' => ['administrador' => 'administrador', 'usuario' => 'usuario'], 'default' => 'usuario']);
        } else {
            echo $this->form->input('rol',['value' => 'usuario', 'readonly' => 'readonly']);
        }
            echo $this->Form->input('nombre');
            echo $this->Form->input('apellido');
            echo $this->Form->input('clave', ['type' => 'password']);
            echo $this->Form->input('creado',['type' => 'hidden']);
            echo $this->Form->input('modificado',['type' => 'hidden']);
            echo $this->Form->input('por', ['type' => 'hidden', 'value' => $this->request->session()->read('Auth.User.username')]);
        ?>

    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>