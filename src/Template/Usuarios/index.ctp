<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Crear Usuario'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link( ('Listar Marcadores'), ['controller' => 'Marcadores', 'action' => 'index'])

         ?></li>
    </ul>
</nav>
<div class="usuarios index large-9 medium-8 columns content">
    <h3><?= __('Usuarios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('username') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('nombre') ?></th>
                <th><?= $this->Paginator->sort('apellido') ?></th>
                <th class="actions"><?= $this->Html->image(__('add2.png'), ['url' => ['action' => 'add'], 'class' => 'agregar']) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?= $this->Number->format($usuario->id) ?></td>
                <td><?= h($usuario->username) ?></td>
                <td><?= h($usuario->email) ?></td>
                <td><?= h($usuario->nombre) ?></td>
                <td><?= h($usuario->apellido) ?></td>
                <td class="actions">
                    <?= $this->Html->image(__('detail.png'), ['url' => ['action' => 'view', $usuario->id], 'class' => 'detalles' ]) ?>
                    <?= $this->Html->image(__('Edit.png'), ['url' => ['action' => 'edit', $usuario->id], 'class' => 'modificar' ]) ?>
                    <?= $this->Form->postLink(
                    $this->Html->image('delete.png', ['class' => 'borrar']), ['action' => 'delete', $usuario->id], ['escape' => false, 'confirm' => __('Seguro que quieres borrar al usuario "{0}"?', $usuario->username)])?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter('{{page}} de {{pages}}') ?></p>
    </div>
</div>

