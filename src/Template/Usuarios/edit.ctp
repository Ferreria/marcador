<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
    <?php
     if ($username == $usuario->username) { ?>
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'delete', $usuario->id],
                ['confirm' => __('Seguro que quieres eliminar tu cuenta?')]
            )
        ?></li>
    <?php
     } else { ?>
            <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'delete', $usuario->id],
                ['confirm' => __('Seguro que quieres eliminar al usuario "{0}"? ', $usuario->username)]
            )
        ?></li>
    <?php } ?>
    <?php
            if ($rol == "administrador") { ?>
        <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?></li>
    <?php } ?>
        <li><?= $this->Html->link ( ('Listar Marcadores'), ['controller' => 'Marcadores', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Editar Usuario') ?></legend>
        <?php
            if ($rol == "administrador") {
                echo $this->Form->input('username');
            } else { 
            echo $this->Form->input('username', ['readonly' => "readonly"]);
        }
            echo $this->Form->input('nombre');
            echo $this->Form->input('apellido');
            echo $this->Form->input('email');
        if ($rol == 'administrador') {
            echo $this->form->input('rol',['options' => ['administrador' => 'administrador', 'usuario' => 'usuario'], 'default' => $default]);
        }
            echo $this->Form->input('clave', ['type' => 'password']);
            echo $this->Form->input('modificado');
            echo $this->Form->input('por', ['value' => $this->request->session()->read('Auth.User.username'), 'type' => 'hidden']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Modificar')) ?>
    <?= $this->Form->end() ?>
</div>
