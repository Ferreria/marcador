<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
     <?= $url=$this->fetch('title') ?>

    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->script("jquery")   ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

<script type="text/javascript">
    $(document).ready(function () {

        $(".side-nav,.user-conf").hide();
     var flag = 0;

        $(".menu").click(function () {
            $(this).toggleClass('sombrear');
            $(".side-nav").slideToggle(500);
         });

        $(".buscar,.username,.logout,.profile").hover(function () {
            $(this).toggleClass('sombrear');
         });



        $(".username").click(function () { 
        if (flag == 0) { 
            $(".user-conf").show();
            $(".user-conf").animate({left: '+=100px'});
         flag = 1;
        } else {
            $(".user-conf").animate({left: '-=100px'},300);
            $(".user-conf").toggle("slow");
        flag = 0;
        }
            });

         $(".buscar").click(function() {
            var str = document.getElementById("search").value;
                if (str.length > 0) {
                    var res = str.replace(/,/g,"/");
                    document.getElementById('search').value = res;
                    location.href='/marcadores/etiquetado/' +document.getElementById('search').value;
                } else {
                     alert("Introduce una categoria");
                }
        });

        $('#search').keypress(function(e){
            if(e.which == 13){
                $('.buscar').click();
            }
        });

        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
        }
        });

        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });


    });   

     </script> 
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li>
                <h1><?= $this->Html->link(__($url), ['controller' => $url, 'action' => 'index'], ['class' => 'titulo'])?></h1>
                
            </li>
        </ul>
<div class="top-bar-section">
<p  class="username">
<?=  $nombre = $this->request->session()->read('Auth.User.username') ?>
</p>
</div>
<?php if (isset($nombre)) { ?>
        <div class="user-conf">
                <?=  $this->Html->link($this->Html->image('Edit_user.png'),['controller' => 'usuarios', 'action' => 'edit', $this->request->session()->read('Auth.User.id')], ['class' => 'profile','escape' => false] ) ?>
                <?= $this->Html->image('logout.png', ['url' => '/usuarios/logout', 'class' => 'logout']); ?>
        </div>
<?php } ?>
    </nav>
    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth', ['message' => 'No tienes acceso para hacer eso.']) ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>



<?php 

        if(!empty($nombre)) {
            echo $this->Form->input('link', ['placeholder' => 'Buscar etiqueta' ,'label' => false ,'id' => 'search', 'class' => 'texto']);
            echo ' </div> ';
            echo $this->Html->image('search-button-icon-93111.png', [ 'class' => 'buscar']);

            echo $this->Html->image('menu.png', ['class' => 'menu']);
        }
?> 

<?= $this->Html->image('arrow.png',['url' => '#', 'class' => 'scrollToTop'])?>

</body>
</html>
