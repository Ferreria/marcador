<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">

        <li><?= $this->Html->link(__('Crear Etiqueta'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Marcadores'), ['controller' => 'Marcadores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Crear Marcador'), ['controller' => 'Marcadores', 'action' => 'add']) ?></li>
                <?php
        if (isset($boss))  { ?>
        <li><?=  $this->Html->link (('Crear usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
         <li><?=  $this->Html->link (('Listar usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
        <?php } ?>  
    </ul>
</nav>
<div class="etiquetas index large-9 medium-8 columns content">
    <h3><?= __('Etiquetas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Creador') ?></th>
                <th><?= $this->Paginator->sort('titulo') ?></th>
                <th><?= $this->Paginator->sort('creado') ?></th>
                <th><?= $this->Paginator->sort('modificado') ?></th>
                <th class="actions"><?= $this->Html->image(__('add2.png'), ['url' => ['action' => 'add'], 'class' => 'agregar']) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($etiquetas as $etiqueta): ?>
            <tr>
                <td><?= $this->Number->format($etiqueta->id) ?></td> 
                <td><?= h($etiqueta->dueño)?></td>               
                <td><?= h($etiqueta->titulo) ?></td>
                <td><?= h($etiqueta->creado) ?></td>
                <td><?= h($etiqueta->modificado) ?></td>
                <td class="actions">
                    <?= $this->Html->image(__('detail.png'), ['url' => ['controller' => 'Etiquetas', 'action' => 'view', $etiqueta->id], 'class' => 'detalles' ]) ?>
                    <?= $this->Html->image(__('Edit.png'), ['url' => ['controller' => 'Etiquetas', 'action' => 'edit', $etiqueta->id], 'class' => 'modificar' ]) ?>
                    <?= $this->Form->postLink($this->Html->image('delete.png', ['class' => 'borrar']),['action' => 'delete', $etiqueta->id], ['escape' => false, 'confirm' => __('Seguro que quieres borrar la etiqueta "{0}" ?', $etiqueta->titulo)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter('{{page}} de {{pages}}') ?></p>
    </div>
</div>
