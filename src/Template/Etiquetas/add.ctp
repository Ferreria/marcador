<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">

        <li><?= $this->Html->link(__('Listar Etiquetas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Marcadores'), ['controller' => 'Marcadores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Crear Marcador'), ['controller' => 'Marcadores', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="etiquetas form large-9 medium-8 columns content">
    <?= $this->Form->create($etiqueta) ?>
    <fieldset>
        <legend><?= __('Agregar Etiqueta') ?></legend>
        <?php
        
            echo $this->Form->input('titulo');
            echo $this->Form->input('marcadores._ids', ['options' => $marcadores]);
            echo $this->Form->input('creado');
            echo $this->Form->input('modificado');
            echo $this->Form->input('dueño', ['type' => 'hidden', 'value' => $this->request->session()->read('Auth.User.username')]);
            echo $this->Form->input('por', ['type' => 'hidden', 'value' => $this->request->session()->read('Auth.User.username')]);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
