<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $etiqueta->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $etiqueta->id], ['confirm' => __('Seguro que quieres borrar el: # {0}?', $etiqueta->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Etiquetas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Crear Etiqueta'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Marcadores'), ['controller' => 'Marcadores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Crear Marcador'), ['controller' => 'Marcadores', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="etiquetas view large-9 medium-8 columns content">
    <h3><?= h($etiqueta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Titulo') ?></th>
            <td><?= h($etiqueta->titulo) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($etiqueta->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Dueño') ?></th>
            <td><?= h($etiqueta->dueño) ?></td>
        </tr>        
        <tr>        
            <th><?= __('Creado') ?></th>
            <td><?= h($etiqueta->creado) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado') ?></th>
            <td><?= h($etiqueta->modificado) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado por') ?></th>
            <td><?= h($etiqueta->por) ?></td>
        </tr>        
    </table>
    <div class="related">
        <h4><?= __('Marcadores relacionados') ?></h4>
        <?php if (!empty($etiqueta->marcadores)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Id Usuario') ?></th>
                <th><?= __('Titulo') ?></th>
                <th><?= __('Descripción') ?></th>
                <th><?= __('Url') ?></th>
                <th><?= __('Creado') ?></th>
                <th><?= __('Modificado') ?></th>
                <th class="actions"><?= __('Opciones') ?></th>
            </tr>
            <?php foreach ($etiqueta->marcadores as $marcadores): ?>
            <tr>
                <td><?= h($marcadores->id) ?></td>
                <td><?= h($marcadores->id_usuario) ?></td>
                <td><?= h($marcadores->titulo) ?></td>
                <td><?= h($marcadores->descripción) ?></td>
                <td><?= h($marcadores->url) ?></td>
                <td><?= h($marcadores->creado) ?></td>
                <td><?= h($marcadores->modificado) ?></td>
                <td class="actions">
                    <?= $this->Html->image(__('detail.png'), ['url' => ['controller' => 'Marcadores', 'action' => 'view', $marcadores->id], 'class' => 'detalles' ]) ?>
                   <?= $this->Html->image(__('Edit.png'), ['url' => ['controller' => 'Marcadores', 'action' => 'edit', $marcadores->id], 'class' => 'modificar' ]) ?>
                    <?= $this->Form->postLink(
                    $this->Html->image('delete.png', ['class' => 'borrar']), ['controller' => 'Marcadores','action' => 'delete', $marcadores->id], ['escape' => false, 'confirm' => __('Seguro que quieres eliminar el marcador: # {0}?', $marcadores->id)])?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
