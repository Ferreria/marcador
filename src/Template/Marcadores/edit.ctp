<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Borrar'),
                ['action' => 'delete', $marcadore->id],
                ['confirm' => __('Seguro que quieres borrar el marcador: "{0}"?', $marcadore->titulo)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Marcadores'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Etiquetas'), ['controller' => 'Etiquetas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nueva Etiqueta'), ['controller' => 'Etiquetas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="marcadores form large-9 medium-8 columns content">
    <?= $this->Form->create($marcadore) ?>
    <fieldset>
        <legend><?= __('Editar Marcador') ?></legend>
        <?php
            echo $this->Form->input('titulo');
            echo $this->Form->input('descripción');
            echo $this->Form->input('url',['type' => 'url']);
            echo $this->Form->input('etiquetas._ids', ['options' => $etiquetas]);
            echo $this->Form->input('por', ['type' => 'hidden', 'value' => $this->request->session()->read('Auth.User.username')]);
              echo $this->Form->input('modificado',['type' => 'hidden']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Form->end() ?>
</div>
