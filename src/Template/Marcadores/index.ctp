<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Crear Marcador'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar tus marcadores'), ['action' => 'owner']) ?></li>
        <li><?= $this->Html->link(__('Listar Etiquetas'), ['controller' => 'Etiquetas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Crear Etiqueta'), ['controller' => 'Etiquetas', 'action' => 'add']) ?></li>

        <?php
        if (isset($admin)) { ?>
            <li><?=  $this->Html->link (('Crear usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>

            <li><?=  $this->Html->link (('Listar usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?></li>
        <?php } ?>  
        
    </ul>
</nav>
<div class="marcadores index large-9 medium-8 columns content">
    <h3><?= __('Marcadores') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Creado por') ?></th>
                <th><?= $this->Paginator->sort('titulo') ?></th>
                <th><?= $this->Paginator->sort('creado') ?></th>
                <th><?= $this->Paginator->sort('modificado') ?></th>
                <th class="actions"><?= $this->Html->image(__('add2.png'), ['url' => ['action' => 'add'], 'class' => 'agregar']) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($marcadores as $marcadore): ?>
            <tr>
                <td><?= $this->Number->format($marcadore->id) ?></td>
                <td><?= h($marcadore->dueño) ?></td>
                <td><?= $this->Html->link($marcadore->titulo,$marcadore->url,['target' => '_blank']) ?></td>
                <td><?= h($marcadore->creado) ?></td>
                <td><?= h($marcadore->modificado) ?></td>
                <td class="actions">
                    <?= $this->Html->image(__('detail.png'), ['url' => ['action' => 'view', $marcadore->id], 'class' => 'detalles' ]) ?>
                    <?= $this->Html->image(__('Edit.png'), ['url' => ['action' => 'edit', $marcadore->id], 'class' => 'modificar' ]) ?>
                    <?= $this->Form->postLink(
                    $this->Html->image('delete.png', ['class' => 'borrar']), ['action' => 'delete', $marcadore->id], ['escape' => false, 'confirm' => __('Seguro que quieres borrar el marcador "{0}"?', $marcadore->titulo)])?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter('{{page}} de {{pages}}') ?></p>

    </div>
</div>
