<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">

        <li><?= $this->Html->link(__('Listar Marcador'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Etiquetas'), ['controller' => 'Etiquetas', 'action' => 'index']) ?></li>
    </ul>
</nav>

<div class="index large-9 medium-8 columns content">
<?php  if ($rows > 0) { ?>
<h3>
    Marcadores etiquetados con:
    <?= $this->Text->toList($tags,$and='y') ?>
</h3>

<section>
<?php $contador = 1; ?>
<?php foreach ($marcadores as $marcadore): ?>
    <article>
    
        <!-- Use the HtmlHelper to create a link -->
        <h3><?= $contador,('. '), $this->Html->link($marcadore->titulo, $marcadore->url,['target' => '_blank']) ?></h3>
        <p><small><?= h($marcadore->url) ?></small></p>

        <!-- Use the TextHelper to format text -->
        <?= $this->Text->autoParagraph($marcadore->descripción) ?>
        <div class="linea">
        </div>

    </article>
 <?php $contador++; ?>
<?php endforeach; ?>
</section>
</div>
<?php } else { ?>
    <h3> No se han encontrado ningún resultado por:
        <?= $this->Text->toList($tags,$and='y') ?>
    </h3> 
</div>
<?php } ?>