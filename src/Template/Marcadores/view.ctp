<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $marcadore->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $marcadore->id], ['confirm' => __('Seguro que quieres borrar este marcador?', $marcadore->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Marcadores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Crear Marcador'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Etiquetas'), ['controller' => 'Etiquetas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Crear Etiqueta'), ['controller' => 'Etiquetas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="marcadores view large-9 medium-8 columns content">
    <h3><?= h($marcadore->titulo) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Titulo') ?></th>
            <td><?= h($marcadore->titulo) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($marcadore->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Creado') ?></th>
            <td><?= h($marcadore->creado) ?></td>
        </tr>
        <tr>
            <th><?= __('Creado por') ?></th>
            <td><?= h($marcadore->dueño) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado') ?></th>
            <td><?= h($marcadore->modificado) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado por') ?></th>
            <td><?= h($marcadore->por) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descripción') ?></h4>
        <?= $this->Text->autoParagraph(h($marcadore->descripción)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url') ?></h4>
        <?= $this->Html->link($marcadore->url); ?>
    </div>
    <div class="related">
        <h4><?= __('Etiquetas relacionadas') ?></h4>
        <?php if (!empty($marcadore->etiquetas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Titulo') ?></th>
                <th><?= __('Creado') ?></th>
                <th><?= __('Modificado') ?></th>
                <th class="actions"><?= __('Opciones') ?></th>
            </tr>
            <?php foreach ($marcadore->etiquetas as $etiquetas): ?>
            <tr>
                <td><?= h($etiquetas->id) ?></td>
                <td><?= h($etiquetas->titulo) ?></td>
                <td><?= h($etiquetas->creado) ?></td>
                <td><?= h($etiquetas->modificado) ?></td>
                <td class="actions">
                     <?= $this->Html->image(__('detail.png'), ['url' => ['controller' => 'Etiquetas','action' => 'view', $etiquetas->id], 'class' => 'detalles' ]) ?>
                    <?= $this->Html->image(__('Edit.png'), ['url' => ['controller' => 'Etiquetas','action' => 'edit', $etiquetas->id], 'class' => 'modificar' ]) ?>
                    <?= $this->Form->postLink(
                    $this->Html->image('delete.png', ['class' => 'borrar']), ['controller' => 'Etiquetas','action' => 'delete', $marcadore->id], ['escape' => false, 'confirm' => __('Seguro que quieres borrar la etiqueta # {0}?', $marcadore->id)])?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
