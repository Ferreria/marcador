<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Etiquetas Controller
 *
 * @property \App\Model\Table\EtiquetasTable $Etiquetas
 */
class EtiquetasController extends AppController
{

public function isAuthorized($user)
{
        if ($user['rol'] == 'administrador') {
        $this->set('boss', 'admin@admin.com');
        return true;
    }
    $action = $this->request->params['action'];

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'tags','view'])) {
        return true;
    }
    // All other actions require an id.
    if (empty($this->request->params['pass'][0])) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->params['pass'][0];
    $etiqueta = $this->Etiquetas->get($id);
    if ($etiqueta->id_usuario == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $etiquetas = $this->paginate($this->Etiquetas);

        $this->set(compact('etiquetas'));
        $this->set('_serialize', ['etiquetas']);
    }

    /**
     * View method
     *
     * @param string|null $id Etiqueta id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $etiqueta = $this->Etiquetas->get($id, [
            'contain' => ['Marcadores']
        ]);

        $this->set('etiqueta', $etiqueta);
        $this->set('_serialize', ['etiqueta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $etiqueta = $this->Etiquetas->newEntity();
        if ($this->request->is('post')) {
            $etiqueta = $this->Etiquetas->patchEntity($etiqueta, $this->request->data);
            $etiqueta->id_usuario = $this->Auth->user('id');
            if ($this->Etiquetas->save($etiqueta)) {
                $this->Flash->success(__('La etiqueta ha sido guardada.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('No se ha podido guardar la etiqueta.'));
            }
        }
        $marcadores = $this->Etiquetas->Marcadores->find('list', ['limit' => 200]);
        $this->set(compact('etiqueta', 'marcadores'));
        $this->set('_serialize', ['etiqueta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Etiqueta id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $etiqueta = $this->Etiquetas->get($id, [
            'contain' => ['Marcadores']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $etiqueta = $this->Etiquetas->patchEntity($etiqueta, $this->request->data);
            if ($this->Etiquetas->save($etiqueta)) {
                $this->Flash->success(__('La etiqueta ha sido modificada.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('No se ha podido modificar la etiqueta.'));
            }
        }
        $marcadores = $this->Etiquetas->Marcadores->find('list', ['limit' => 200]);
        $this->set(compact('etiqueta', 'marcadores'));
        $this->set('_serialize', ['etiqueta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Etiqueta id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $etiqueta = $this->Etiquetas->get($id);
        if ($this->Etiquetas->delete($etiqueta)) {
            $this->Flash->success(__('La etiqueta ha sido borrada'));
        } else {
            $this->Flash->error(__('No se ha podido borrar la etiqueta'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
