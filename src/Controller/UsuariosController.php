<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Illuminate\Support\Facades\Hash;
use Cake\Validation\Validation;
/**
 * Usuarios Controller
 *
 * @property \App\Model\Table\UsuariosTable $Usuarios
 */
class UsuariosController extends AppController
{
public function isAuthorized($user)
{
        if ($user['rol'] == 'administrador') {
        return true;
    }
        if (empty($this->request->params['pass'][0])) {
        $this->redirect($this->Auth->redirectUrl('/marcadores'));
        return false;
    }
        $id = $this->request->params['pass'][0];
        $usuario = $this->Usuarios->get($id);
        if ($usuario->id == $user['id']) {
        return true;
    }

        return parent::isAuthorized($user);
    }
    
public function initialize()
{
    parent::initialize();
    $this->Auth->allow(['logout','add']);
}

public function logout()
{
    $this->Flash->success('Te has deslogeado correctamente.');
    return $this->redirect($this->Auth->logout());
}

public function login()
{
    if ($this->request->is('post')) {
            if (Validation::email($this->request->data['username'])) {

                $this->Auth->config('authenticate', [
                    'Form' => [
                        'fields' => ['username' => 'email']
                    ]
                ]);

                $this->Auth->constructAuthenticate();
                $this->request->data['email'] = $this->request->data['username'];
                unset($this->request->data['username']);
            }

        $usuario = $this->Auth->identify();
            if ($usuario) {
                 $this->Auth->setUser($usuario);
              $username = $this->request->session()->read('Auth.User.username');
                $this->Flash->success('Bienvenido '.$username);
                return $this->redirect($this->Auth->redirectUrl('/marcadores'));

    }
        $this->Flash->error('El correo o la clave es incorrecta.');
    }
}
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $usuarios = $this->paginate($this->Usuarios);

        $this->set(compact('usuarios'));
        $this->set('_serialize', ['usuarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => ['Marcadores']
        ]);

        $this->set('usuario', $usuario);
        $this->set('_serialize', ['usuario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if (empty($this->request->session()->read('Auth.User.username')) | $this->request->session()->read('Auth.User.username') == 'admin') {
          $username = $this->request->session()->read('Auth.User.username');
          $rol = $this->request->session()->read('Auth.User.rol');
          $this->set('username', $username);
          $this->set('rol', $rol);
        $usuario = $this->Usuarios->newEntity();
        if ($this->request->is('post')) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->data);
            if ($this->Usuarios->save($usuario)) {
              $email = $this->request->session()->read('Auth.User.email');
              
                        if ($email == 'admin@admin.com') { 
                $this->Flash->success(__('El usuario se ha agregado correctamente.'));               
                     
                            return $this->redirect(['controller' => 'usuarios','action' => 'index']);
                        } else {
                            $usuario = $this->Auth->identify();
                            $this->Auth->setUser($usuario);
                        $nombre = $this->request->session()->read('Auth.User.username');
                            $this->Flash->success('Bienvenido '.$nombre);
                            return $this->redirect($this->Auth->redirectUrl('/marcadores'));
                    }
                
            } else {
                $this->Flash->error(__('No se pudo agregar al usuario.'));
                return $this->redirect(['controller' => 'usuarios','action' => 'login']);
            }
        }
        $this->set(compact('usuario'));
        $this->set('_serialize', ['usuario']);
    } else {
      $this->Flash->error('Deslogeate para poder crear una cuenta');
      $this->redirect($this->referer());
    }

  }

    /**
     * Edit method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $username = $this->request->session()->read('Auth.User.username');
        $rol = $this->request->session()->read('Auth.User.rol');
        $usuario = $this->Usuarios->get($id, [
            'contain' => []
        ]);
                    $default = $usuario['rol'];
            $this->set('default', $default);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->data);

            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('Se ha modificado el usuario.'));
                if ($rol == 'administrador') {
                    $this->setAction('index');
                } else {
                $this->redirect('/');
            }
            } else {
                $this->Flash->error(__('No se ha podido modificar al usuario.'));
                $this->redirect('/');
            }
        }
        $this->set(compact('usuario'));
        $this->set('_serialize', ['usuario']);
        $this->set('rol', $rol);
        $this->set('username', $username);
    }

    /**
     * Delete method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usuario = $this->Usuarios->get($id);
        $username = $this->request->session()->read('Auth.User.username');

        if ($this->Usuarios->delete($usuario)) {
          if ($username == "admin") {
            $this->Flash->success(__('El usuario ha sido eliminado.'));
          } else {

            $this->redirect($this->Auth->logout());
            $this->Flash->success(__('El usuario ha sido eliminado.'));
        }
         } else {
            $this->Flash->error(__('No se ha podido eliminar al usuario'));
        }
        return $this->redirect(['action' => 'index']);
    }

    
}
