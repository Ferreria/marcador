<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Marcadores Controller
 *
 * @property \App\Model\Table\MarcadoresTable $Marcadores
 */
class MarcadoresController extends AppController
{

public function isAuthorized($user)
{

    if ($user['rol'] == 'administrador') {

        $this->set('admin', $user['rol']);
        return true;
    }

    $action = $this->request->params['action'];

    // The add and index actions are always allowed.
    if (in_array($action, ['index', 'add', 'view', 'tags', 'owner'])) {
        return true;
    }
    // All other actions require an id.
    if (empty($this->request->params['pass'][0])) {
        return false;
    }

    // Check that the bookmark belongs to the current user.
    $id = $this->request->params['pass'][0];

    $marcador = $this->Marcadores->get($id);
    if ($marcador->id_usuario == $user['id']) {
        return true;
    }
    return parent::isAuthorized($user);
}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

public function owner()
{
    $this->paginate = [
        'conditions' => [
            'Marcadores.id_usuario' => $this->Auth->user('id'),
        ]
    ];
    $marcadores = $this->paginate($this->Marcadores);
    $rows = $marcadores->count();

    $this->set('marcadores', $this->paginate($this->Marcadores));
    $this->set('_serialize', ['marcadores']);
    $this->set('rows', $rows);
}


    public function index()
    {
        $this->paginate = [ 
            'contain' => ['Usuarios']
            ];
        $marcadores = $this->paginate($this->Marcadores);
        $this->set(compact('marcadores'));
        $this->set('_serialize', ['marcadores']);
         $this->set('url', 'marcadores' );
    }

    /**
     * View method
     *
     * @param string|null $id Marcadore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $marcadore = $this->Marcadores->get($id, [
            'contain' => ['Etiquetas', 'Usuarios']
        ]);

        $this->set('marcadore', $marcadore);
        $this->set('_serialize', ['marcadore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $marcadore = $this->Marcadores->newEntity();
        if ($this->request->is('post')) {
            $marcadore = $this->Marcadores->patchEntity($marcadore, $this->request->data);
             $marcadore->id_usuario = $this->Auth->user('id');
            if ($this->Marcadores->save($marcadore)) {
                $this->Flash->success(__('Marcador agregado'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('No se ha podido agregar el marcador.'));
            }
                $tags = $this->Marcadores->Etiquetas->find('list');
                $this->set(compact('marcador', 'tags'));
                $this->set('_serialize', ['marcador']);
        }
        $username = $this->request->session()->read('Auth.User.username');
        $etiquetas = $this->Marcadores->Etiquetas->find('list', ['limit' => 200]);
        $this->set(compact('marcadore', 'etiquetas', 'usuarios'));
        $this->set('_serialize', ['marcadore']);
        $this->set('username', $username);
    }

    /**
     * Edit method
     *
     * @param string|null $id Marcadore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $marcadore = $this->Marcadores->get($id, [
            'contain' => ['Etiquetas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $marcadore = $this->Marcadores->patchEntity($marcadore, $this->request->data);
            $marcadore->id_usuario = $this->Auth->user('id');
            if ($this->Marcadores->save($marcadore)) {
                $this->Flash->success(__('El marcador ha sido modificado.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('No se ha podido modificar el marcador.'));
            }
               $tags = $this->Marcadores->Tags->find('list');
                $this->set(compact('marcador', 'tags'));
                $this->set('_serialize', ['marcador']);
        }
        $etiquetas = $this->Marcadores->Etiquetas->find('list', ['limit' => 200]);
        $this->set(compact('marcadore', 'etiquetas', 'usuarios'));
        $this->set('_serialize', ['marcadore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Marcadore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $marcadore = $this->Marcadores->get($id);
        if ($this->Marcadores->delete($marcadore)) {
            $this->Flash->success(__('Marcador borrado.'));
        } else {
            $this->Flash->error(__('No se ha podido borrar el marcador.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function tags()
{
    // The 'pass' key is provided by CakePHP and contains all
    // the passed URL path segments in the request.
    $tags = $this->request->params['pass'];

    // Use the BookmarksTable to find tagged bookmarks.
    $marcadores = $this->Marcadores->find('etiquetado', [
        'tags' => $tags
    ]);

    $rows = $marcadores->count();

    // Pass variables into the view template context.
    $this->set([
        'marcadores' => $marcadores,
        'tags' => $tags,
        'rows' => $rows
    ]);

}
}
