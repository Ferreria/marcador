<?php
namespace App\Model\Table;

use App\Model\Entity\Etiqueta;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Etiquetas Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Marcadores
 */
class EtiquetasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('etiquetas');
        $this->displayField('titulo');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp', ['events' => ['Model.beforeSave' => [ 'creado' => 'new', 'modificado' => 'always']]]);

        $this->belongsTo('Usuarios', [
          'foreignKey' => 'id_usuario',
          'joinType' => 'INNER'
        ]);

        $this->belongsToMany('Marcadores', [
            'foreignKey' => 'id_etiqueta',
            'targetForeignKey' => 'id_marcador',
            'joinTable' => 'marcadores_etiquetas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('titulo')
            ->add('titulo', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['titulo']));
        $rules->add($rules->existsIn(['id_usuario'], 'Usuarios'));
        return $rules;
    }
}
