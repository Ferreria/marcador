<?php
namespace App\Model\Table;

use App\Model\Entity\Marcadore;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Marcadores Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Etiquetas
 */
class MarcadoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('marcadores');
        $this->displayField('titulo');
        $this->primaryKey('id');

         $this->addBehavior('Timestamp', ['events' => ['Model.beforeSave' => [ 'creado' => 'new', 'modificado' => 'always']]]);

         $this->belongsTo('Usuarios', [
            'foreignKey' => 'id_usuario',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Etiquetas', [
            'foreignKey' => 'id_marcador',
            'targetForeignKey' => 'id_etiqueta',
            'joinTable' => 'marcadores_etiquetas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('titulo');

        $validator
            ->allowEmpty('descripción');

        $validator
            ->add('url', 'validFormat',['rule' => 'url', 'message' => 'Agrega http/s al comienzo']);

        $validator
            ->notEmpty('dueño', 'create');

        $validator
            ->notEmpty('por', 'update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['id_usuario'], 'Usuarios'));
        return $rules;
    }

    // The $query argument is a query builder instance.
// The $options array will contain the 'tags' option we passed
// to find('tagged') in our controller action.
public function findEtiquetado(Query $query, array $options)
{
    return $this->find()
        ->distinct(['id_marcador'])
        ->matching('Etiquetas', function ($q) use ($options) {
            if (empty($options['tags'])) {
                return $q->where(['Etiquetas.titulo IS' => null]);
            }
            return $q->where(['Etiquetas.titulo IN' => $options['tags']]);
        });
}


}
