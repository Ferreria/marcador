-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-05-2016 a las 10:03:25
-- Versión del servidor: 5.5.47-0+deb7u1
-- Versión de PHP: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cake_marcadores`
--
CREATE DATABASE IF NOT EXISTS `cake_marcadores` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cake_marcadores`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetas`
--

DROP TABLE IF EXISTS `etiquetas`;
CREATE TABLE IF NOT EXISTS `etiquetas` (
`id` int(11) NOT NULL,
  `id_usuario` int(200) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `modificado` datetime DEFAULT NULL,
  `dueño` varchar(50) NOT NULL,
  `por` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etiquetas`
--

INSERT INTO `etiquetas` (`id`, `id_usuario`, `titulo`, `creado`, `modificado`, `dueño`, `por`) VALUES
(1, 1, 'Educación', '2016-05-26 09:59:00', '2016-05-26 09:59:00', 'admin', 'admin'),
(2, 1, 'Guarradas', '2016-05-26 09:59:00', '2016-05-26 09:59:00', 'admin', 'admin'),
(3, 1, 'Cocina', '2016-05-26 09:59:00', '2016-05-26 09:59:00', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcadores`
--

DROP TABLE IF EXISTS `marcadores`;
CREATE TABLE IF NOT EXISTS `marcadores` (
`id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `descripción` text,
  `url` text,
  `dueño` varchar(200) NOT NULL,
  `creado` datetime DEFAULT NULL,
  `modificado` datetime DEFAULT NULL,
  `por` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcadores`
--

INSERT INTO `marcadores` (`id`, `id_usuario`, `titulo`, `descripción`, `url`, `dueño`, `creado`, `modificado`, `por`) VALUES
(1, 1, 'Asir las galletas', 'Instituto de educación', 'http://www.asirlasgalletas.com/', 'admin', '2016-05-26 09:56:58', '2016-05-26 09:56:58', 'admin'),
(2, 1, 'Páginas XXX', 'Para pasar el tiempo...', 'http://lmgtfy.com/?q=No+seas+guarro', 'admin', '2016-05-26 09:58:35', '2016-05-26 09:58:35', 'admin'),
(3, 1, 'Cómo preparar gofio', 'Receta para preparar un plato de gofio', 'https://cookpad.com/es/buscar/gofio', 'admin', '2016-05-26 09:59:23', '2016-05-26 09:59:23', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcadores_etiquetas`
--

DROP TABLE IF EXISTS `marcadores_etiquetas`;
CREATE TABLE IF NOT EXISTS `marcadores_etiquetas` (
  `id_marcador` int(11) NOT NULL,
  `id_etiqueta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcadores_etiquetas`
--

INSERT INTO `marcadores_etiquetas` (`id_marcador`, `id_etiqueta`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `rol` varchar(200) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `creado` datetime DEFAULT NULL,
  `modificado` datetime DEFAULT NULL,
  `por` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `email`, `clave`, `rol`, `nombre`, `apellido`, `creado`, `modificado`, `por`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$W2lPyaZvwRuWpLCBHsleqe0NYFcqDmxk0KB.kQHqolslmvb52.kVS', 'administrador', 'Daniel', 'Ferreria', '2016-05-16 00:00:00', '2016-05-18 15:54:00', 'admin'),
(2, 'test', 'test@test.com', '$2y$10$mzAOm9qc6.IfOvRBg3ijVenWBHmjgQtBQa.fqAhVlhK9HHIW0h15G', 'usuario', 'test', 'test', '2016-05-26 10:02:04', '2016-05-26 10:02:04', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `titulo` (`titulo`);

--
-- Indices de la tabla `marcadores`
--
ALTER TABLE `marcadores`
 ADD PRIMARY KEY (`id`), ADD KEY `user_key` (`id_usuario`);

--
-- Indices de la tabla `marcadores_etiquetas`
--
ALTER TABLE `marcadores_etiquetas`
 ADD PRIMARY KEY (`id_marcador`,`id_etiqueta`), ADD KEY `tag_key` (`id_etiqueta`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `marcadores`
--
ALTER TABLE `marcadores`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `marcadores`
--
ALTER TABLE `marcadores`
ADD CONSTRAINT `marcadores_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `marcadores_etiquetas`
--
ALTER TABLE `marcadores_etiquetas`
ADD CONSTRAINT `marcadores_etiquetas_ibfk_1` FOREIGN KEY (`id_etiqueta`) REFERENCES `etiquetas` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `marcadores_etiquetas_ibfk_2` FOREIGN KEY (`id_marcador`) REFERENCES `marcadores` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
