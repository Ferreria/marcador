<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MarcadoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MarcadoresTable Test Case
 */
class MarcadoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MarcadoresTable
     */
    public $Marcadores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.marcadores',
        'app.etiquetas',
        'app.marcadores_etiquetas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Marcadores') ? [] : ['className' => 'App\Model\Table\MarcadoresTable'];
        $this->Marcadores = TableRegistry::get('Marcadores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Marcadores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
